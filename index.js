const fs = require('fs');
const sqlite3 = require('sqlite3');
const Discord = require('discord.js');
const RustPlus = require('@liamcottle/rustplus.js');
const config = require('./config.json')
const db = new sqlite3.Database('rustplus.db');
const bot = new Discord.Client();
const rustplus = new RustPlus(config.rpData.ip, config.rpData.port, config.rpData.playerId, config.rpData.playerToken);

bot.on('ready', () => {
    console.log(`Logged into Discord as @${bot.user.tag}!`)
    //rustplus.sendTeamMessage('[BOT] Discord Bot Online!');
    db.all("SELECT id FROM subscriptions", (err, rows) => {
        rows.forEach((row) => {
            rustplus.getEntityInfo(row.id, (response) => {
                console.log(`Subscribing to entity ${row.id} | response: ${JSON.stringify(response)}`)
                if(response.response.error) {
                    console.log(response.response.error.error);
                    db.exec(`DELETE FROM subscriptions WHERE id=${row.id}`, (err) => {});
                } 
            })
        })
    })
});

rustplus.on('connected', () => {
    console.log(`Logged into RustPlus, attempting Discord login!`);
    //rustplus.sendTeamMessage('[BOT] Logged into RustPlus!');
    bot.login(config.discord.token);
})

rustplus.on('message', (message) => {
    if (message.broadcast && message.broadcast.entityChanged) {
        db.get(`SELECT * FROM subscriptions WHERE id = ${message.broadcast.entityChanged.entityId}`, (err, res) => {
            if(!res) return;
            if(!message.broadcast.entityChanged.payload.value) return;
            bot.channels.cache.get(res.channelid).send(res.message);
        });
    }
});

bot.on('message', (msg) => {
    const prefix = config.discord.prefix;
    const args = msg.content.slice(prefix.length).trim().split(/ +/g);
    const cmd = args.shift().toLowerCase();
    if (msg.content.toLowerCase().startsWith(prefix)) {
        switch (cmd) {
            case "add":
                if (msg.author.id !== config.discord.ownerID) return msg.channel.send("You can't do that!");
                if (!args[0]) return msg.channel.send("Please provide a valid entity ID");
                if (!args[1]) return msg.channel.send("Please provide a response message!");
                if (!/^[0-9]+$/.test(args[0])) return msg.channel.send("Please provide a valid entity ID");
                db.get(`SELECT * FROM subscriptions WHERE id = ${args[0]}`, (err, res) => {
                    if (res) return msg.channel.send(`Listener already exists in <#${res.channelid}>!`);
                    rustplus.getEntityInfo(args[0], (response) => {
                        //msg.channel.send(JSON.stringify(response));
                        if (response.response.error) return msg.channel.send(`Could not add! Error: \`${response.response.error.error}\``)
                        var newSub = db.prepare("INSERT INTO subscriptions VALUES (?,?,?)");
                        newSub.run(args[0], args.splice(1).join(" "), msg.channel.id);
                        newSub.finalize((err) => {
                            if (err) return msg.channel.send(`There was an error creating that subscription! \`${err}\``);
                            msg.channel.send("Added!");
                        });
                    })
                })
                break;
            case "remove":
                if (msg.author.id !== config.discord.ownerID) return msg.channel.send("You can't do that!");
                if (!args[0]) return msg.channel.send("Please provide a valid entity ID");
                if (!/^[0-9]+$/.test(args[0])) return msg.channel.send("Please provide a valid entity ID");
                db.get(`SELECT * FROM subscriptions WHERE id = ${args[0]}`, (err, res) => {
                    if (!res) return msg.channel.send("Listener doesnt exist!");
                    db.exec(`DELETE FROM subscriptions WHERE id=${args[0]}`, (err) => {
                        if(err) return msg.channel.send(`There was an error deleting that subscription! \`${err}\``);
                        msg.channel.send("Deleted!");
                    });
                });
                break;
            case "list":
                if (msg.author.id !== config.discord.ownerID) return msg.channel.send("You can't do that!");
                db.all(`SELECT * FROM SUBSCRIPTIONS`, (err, rows) => {
                    msg.channel.send(`All Subscriptions!\n\`\`\`javascript\n${JSON.stringify(rows, null, "\t")}\n\`\`\``);
                })
        }
    }
})

rustplus.connect();